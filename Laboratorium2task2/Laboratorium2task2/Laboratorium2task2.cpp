﻿#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <vector>
#include <string>


bool comp(const cv::Vec3b& p1, const cv::Vec3b& p2)
{
    return (p1[0] + p1[1] + p1[2]) < (p2[0] + p2[1] + p2[2]);
}

cv::Vec3b median(std::vector<cv::Vec3b> values)
{
    size_t size = values.size();

    if (size == 0)
    {
        return 0;  // Undefined, really.
    }
    else
    {
        std::sort(values.begin(), values.end(), comp);
        if (size % 2 == 0)
        {
            return (values[size / 2 - 1] + values[size / 2]) / 2;
        }
        else
        {
            return values[size / 2];
        }
    }
}


cv::Mat medianFilter(cv::Mat& img, int n) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;
    cv::Mat_<cv::Vec3b> _R = res;
 
    std::vector <cv::Vec3b> pixelsValue;
    for (int i = n/2; i < img.rows - n/2; i++) {
        for (int j = n/2; j < img.cols - n/2; j++) {

            for (int startI = i - n/2; startI < i + n/2+1; startI++) {
                for (int startJ = j - n/2; startJ < j + n/2+1; startJ++) {
                    pixelsValue.push_back(_I(startI, startJ));
                }
            }
            _R(i, j) = median(pixelsValue);


            pixelsValue.clear();
        }
    }
        

    res = _R;
    return res;
}

cv::Vec3b max(std::vector<cv::Vec3b> values) {
    return *std::max_element(values.begin(), values.end(), comp);
}

cv::Mat maxFilter(cv::Mat& img, int n) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;
    cv::Mat_<cv::Vec3b> _R = res;

    std::vector <cv::Vec3b> pixelsValue;
    for (int i = n / 2; i < img.rows - n / 2; i++) {
        for (int j = n / 2; j < img.cols - n / 2; j++) {

            for (int startI = i - n / 2; startI < i + n / 2 + 1; startI++) {
                for (int startJ = j - n / 2; startJ < j + n / 2 + 1; startJ++) {
                    pixelsValue.push_back(_I(startI, startJ));
                }
            }
            _R(i, j) = max(pixelsValue);


            pixelsValue.clear();
        }
    }


    res = _R;
    return res;
}

cv::Vec3b min(std::vector<cv::Vec3b> values) {
    return *std::min_element(values.begin(), values.end(), comp);
}

cv::Mat minFilter(cv::Mat& img, int n) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;
    cv::Mat_<cv::Vec3b> _R = res;

    std::vector <cv::Vec3b> pixelsValue;
    for (int i = n / 2; i < img.rows - n / 2; i++) {
        for (int j = n / 2; j < img.cols - n / 2; j++) {

            for (int startI = i - n / 2; startI < i + n / 2 + 1; startI++) {
                for (int startJ = j - n / 2; startJ < j + n / 2 + 1; startJ++) {
                    pixelsValue.push_back(_I(startI, startJ));
                }
            }
            _R(i, j) = min(pixelsValue);


            pixelsValue.clear();
        }
    }


    res = _R;
    return res;
}

int main(int, char* []) {
    std::cout << "Start ..." << std::endl;

    cv::Mat image = cv::imread("Lena.bmp");
    int n; 
    int option;
    std::cout << "Give the value of N:" << std::endl;
    std::cin >> n;
    std::cout << "Choose one of the following filtrations: " << std::endl << "1) Median" << std::endl << "2) Max " << std::endl << "3) Min " << std::endl;
    std::cin >> option;
    if (option > 3 || option < 1) {
        option = 1;
        std::cout << "The given value was incorrect. Chosen Median Filter by default" << std::endl;
    }
    cv::Mat res;
    std::string savedFileName;
    switch (option) {
    case 1: 
        std::cout << "Median Filter is working..." << std::endl;
        res = medianFilter(image, n);
        savedFileName = "Lena_MedianN" + std::to_string(n);
        break;
    case 2:
        std::cout << "Max Filter is working..." << std::endl;
        res = maxFilter(image, n);
        savedFileName = "Lena_MaxN" + std::to_string(n);
        break;
    case 3:
        std::cout << "Min Filter is working..." << std::endl;
        res = minFilter(image, n);
        savedFileName = "Lena_MinN" + std::to_string(n);
        break;
    }
    
    cv::imshow(savedFileName, res);
    cv::imwrite("results/" + savedFileName + ".bmp", res);
    cv::waitKey(-1);
    return 0;
}
