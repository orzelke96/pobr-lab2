﻿#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <string>
#include <iostream>


int addValue(int sum, int maskVal) {
    if (sum / maskVal > 255)
        return 255;
    else if (sum / maskVal < 0)
        return 0;
    else
        return sum / maskVal;

}

cv::Mat lowPassFilter(cv::Mat& img) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;
    cv::Mat_<cv::Vec3b> _R = res;
    int mask[5][5] = { {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1} , {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1}, {1, 1, 1, 1, 1} };
    int maskVal = 0;
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            maskVal += mask[i][j];
    
    for (int i = 2; i < img.rows-2; i++)
        for (int j = 2; j < img.cols-2; j++) {
            int sumR = 0;
            int sumG = 0;
            int sumB = 0;
            for (int startI = i - 2; startI < i + 3; startI++) {
                for (int startJ = j - 2; startJ <j + 3; startJ++) {
                    sumR += _I(startI, startJ)[0] * mask[startI - i + 2][startJ - j + 2];
                    sumG += _I(startI, startJ)[1] * mask[startI - i + 2][startJ - j + 2];
                    sumB += _I(startI, startJ)[2] * mask[startI - i + 2][startJ - j + 2];
                }
            }
            _R(i, j)[0] = addValue(sumR, maskVal);
            _R(i, j)[1] = addValue(sumG, maskVal);
            _R(i, j)[2] = addValue(sumB, maskVal);
        }
    
    res = _R;
    return res;
}

cv::Mat highPassFilter(cv::Mat& img) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;
    cv::Mat_<cv::Vec3b> _R = res;
    int mask[5][5] = { {-1, -1, -1, -1, -1}, {-1, -1, -1, -1, -1} , {-1, -1, -8, -1, -1}, {-1, -1, -1, -1, -1}, {-1, -1, -1, -1, -1} };
    int maskVal = 0;
    for (int i = 0; i < 5; i++)
        for (int j = 0; j < 5; j++)
            maskVal += mask[i][j];

    for (int i = 2; i < img.rows - 2; i++)
        for (int j = 2; j < img.cols - 2; j++) {
            int sumR = 0;
            int sumG = 0;
            int sumB = 0;
            for (int startI = i - 2; startI < i + 3; startI++) {
                for (int startJ = j - 2; startJ < j + 3; startJ++) {
                    sumR += _I(startI, startJ)[0] * mask[startI - i + 2][startJ - j + 2];
                    sumG += _I(startI, startJ)[1] * mask[startI - i + 2][startJ - j + 2];
                    sumB += _I(startI, startJ)[2] * mask[startI - i + 2][startJ - j + 2];
                }
            }
            _R(i, j)[0] = addValue(sumR, maskVal);
            _R(i, j)[1] = addValue(sumG, maskVal);
            _R(i, j)[2] = addValue(sumB, maskVal);
        }

    res = _R;
    return res;
}


int main(int, char* []) {
    std::cout << "Start ..." << std::endl;
    int i = 0; // change to different value for the highpass filter
    cv::Mat image = cv::imread("Lena.bmp");
    cv::Mat res = lowPassFilter(image);
    if (i == 0) {
        cv::Mat res = lowPassFilter(image);
        std::string name = "lowPassLena";
        cv::imwrite("results/" + name + ".bmp", res);
    }
    else {
        cv::Mat res = highPassFilter(image);
        std::string name = "highPassLena";
        cv::imwrite("results/" + name + ".bmp", res);
    }

    cv::imshow("LenaAfterEffect", res);
    cv::waitKey(-1);
    return 0;
}
